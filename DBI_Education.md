## 1. Найдите производителей принтеров. Вывести: maker.

```
SELECT maker
FROM   product
WHERE  TYPE = 'Printer' 
```

## 2. Найдите номер модели, объем памяти (RAM) и размер экранов ноутбуков, цена которых превышает 1000 дол. 

```
SELECT code,
       ram,
       screen
FROM   laptop
WHERE  price > 1000 
```

## 3. Найдите номера модели персональных компьютеров (PC), имеющих скорость процессора 450 либо 500 и объем оперативной памяти = 64M. Вывод: модель, скорость, память. Нужно использовать оператор IN.

``` 
SELECT DISTINCT model,
                speed,
                ram
FROM   pc
WHERE  speed IN ( 450, 500 )
       AND ram IN ( 64 ) 
```

## 4. Найдите номера модели персональных компьютеров (PC), у которых номер модели начинается на 126 или цена меньше или равна 970 дол. Вывод: модель, цена.

```
SELECT DISTINCT model,
                price
FROM   pc
WHERE  model LIKE '126%'
        OR price <= '970' 
```

## 5. Нужно найти производителей, которые выпускают и продают ПК. Вывести полное имя производителя и его адрес. Вывод отсортировать по полному названию производителя.

```
SELECT DISTINCT p3.full_name,
                p3.address
FROM   pc p1
       join product p2
         ON p1.model = p2.model
       join contractor p3
         ON p3.maker = p2.maker
WHERE  TYPE = 'PC'
ORDER  BY full_name 
```

## 6. Найдите размеры жестких дисков, совпадающих у двух и более PC. Вывести: HD

```
SELECT hd
FROM   pc
GROUP  BY hd
HAVING Count(model) >= 2 
```
