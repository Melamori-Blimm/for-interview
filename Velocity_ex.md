## 1. Самый простой шаблон Velocity.

[![VST-t1.jpg](https://i.postimg.cc/fLYSF8hR/VST-t1.jpg)](https://postimg.cc/S2NKY7sB)

## 2. Самый простой шаблон Velocity с циклом.

[![VST-t2.jpg](https://i.postimg.cc/d0bT4zpf/VST-t2.jpg)](https://postimg.cc/Z9LRqDTP)

## 3. Пример как генерировать XML при помощи Velocity.

[![VST-t3.jpg](https://i.postimg.cc/SK2MLyT8/VST-t3.jpg)](https://postimg.cc/QBDChrrx)


