## 1. Вывести имя сотрудника и название отдела только для тех сотрудников, которые числятся к каком-либо отделе. Если сотрудник не числится ни в одном отделе - данные по нему не выводить.

```
SELECT emp.NAME,
       dep.department
FROM   employees emp
       LEFT JOIN departments dep
              ON emp.department_id = dep.id
WHERE  dep.department IS NOT NULL
ORDER  BY emp.id 
```

## 2. Вывести следующие данные: название отдела и среднюю зарплату сотрудников в нем. Средняя зарплата должны быть округлена до двух знаков после запятой. Отсортировать по средней з/п, начиная с наибольшей

```
SELECT DISTINCT Round(Avg(emp.salary)
                        OVER (
                          partition BY dep.department), 2) sal,
                dep.department,
                emp.department_id
FROM   employees emp
       JOIN departments dep
         ON emp.department_id = dep.id
ORDER  BY Round(Avg(emp.salary)
                  OVER (
                    partition BY dep.department), 2) DESC 
```

## 3. Вывести следующие данные: имя сотрудника и имя менеджера для него.

```
SELECT NAME,
       manager_id
FROM   employees; 
```

## 4. Посчитать кол-во сотрудников в каждом районе (таблица Location).

```
SELECT DISTINCT Count(emp.NAME)
                  OVER (
                    partition BY location) loc
FROM   employees emp
       LEFT JOIN departments dep
              ON emp.department_id = dep.id
       JOIN locations loc
         ON dep.location_id = loc.id 
```

## 5. Вывести названия отделов, у которых средний возраст сотрудников больше 30 лет.

```
SELECT DISTINCT Round(Avg(DISTINCT emp.age)
                        over (
                          PARTITION BY dep.department ), 2) Age,
                dep.department
FROM   employees emp
       left join departments dep
              ON emp.department_id = dep.id
WHERE  age > 30
ORDER  BY dep.department 
```

## 6. Создать копию таблицы EMPLOYEES и назвать ее EMPLOYEES_BCKP. Изменить в EMPLOYEES_BCKP имя сотрудника Dorofeeva Irina на Alieva Irina.

```
CREATE TABLE employees_bckp AS
  (SELECT *
   FROM   employees) select * FROM employees_bckp;

UPDATE employees_bckp
SET    name = 'Alieva Irina'
WHERE  name = 'Dorofeeva Irina'  
```

## 7. Написать запрос, который сравнит таблицы EMPLOYEES и EMPLOYEES_BCKP и выведет разницу между таблицами.

```
SELECT *
FROM   employees em
WHERE  em.NAME NOT IN (SELECT eb.NAME
                       FROM   employees_bckp eb)

SELECT *
FROM   employees
minus
SELECT *
FROM   employees_bckp 
```

## 10 .Найти дублирующиеся строки в таблице EMPLOYEES. Написать запрос, который будет удалять дубликат строки. (дубликаты - это строки где все поля одинаковые кроме ID)

```
SELECT DISTINCT name,
                hire_date,
                gender,
                age,
                salary,
                manager_id,
                department_id
FROM   employees
ORDER  BY name 
```

