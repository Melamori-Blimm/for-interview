## SQL Tasks on SQL Developer

Подготовила для интервью небольшую часть задач по SQL, которые решали на курсе в компании DBI. 
  
- [ ] [Задачи решенные на курсе.](https://gitlab.com/Melamori-Blimm/for-interview/-/blob/main/DBI_Education.md)
- [ ] [Задачи решенные на экзамене.](https://gitlab.com/Melamori-Blimm/for-interview/-/blob/main/DBI_Exam.md)

## QA

Примеры, которые создала при обучении и на тестовых заданиях при собеседовании.

- [ ] [Test Keys, Check List, Bug Report](https://drive.google.com/drive/folders/1ZhlMyULatTt7cfjgE1qZYUS6NiqWIsjy?usp=sharing)

## Velocity

Примеры, которые создала при ознакомлении с языком Velocity. 

- [ ] [Примеры на Velocity](https://gitlab.com/Melamori-Blimm/for-interview/-/blob/main/Velocity_ex.md)

## Пример телефонной книги на json

- [ ] [Пример на json](https://gitlab.com/Melamori-Blimm/for-interview/-/blob/main/phones.json)

## Пример телефонной книги на xml

- [ ] [Пример на xml](https://gitlab.com/Melamori-Blimm/for-interview/-/blob/main/phones.xml)
